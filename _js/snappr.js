// SNAPPR PLUGIN

(function($) {
	$.fn.snappr = function(options) {
		var wrap = $(this);
		var ran = false;

		var settings = $.extend({
			distance: 5,
			duration: 0.5,
			buffer: 1000,
			animateLoad: false,
			anchors: [],
			navigation: false,
			horizontal: false,
			hideArrows: true,
			touch: true,
			afterLoad: function(current,direction) {},
			onLeave: function(current,direction) {}
		},options);

		// setup on page load.
		var snapSetup = function() {
			// move every snap down but the first
			var snaps = $('.snap').not('.snap:first');
			
			if(settings.horizontal) {
				TweenMax.set(snaps,{x:'100%'});
			} else {
				TweenMax.set(snaps,{y:'100%'});
			}

			// add data attributes to ID every snap element
			var num = 1;

			$('.snap').each(function(){
				$(this).attr('data-snap',num);
				num++;
			});

			// adds an active state class to the first element
			$('.snap:first').addClass('active');

			// adds a class name to the first and last snap element to identify them
			$('.snap:first').addClass('first');
			$('.snap:last').addClass('last');

			// create anchors for each snap element there is
			var i = 0; 

			$('.snap').each(function(){
				if ( settings.anchors[i] == undefined ) {
					settings.anchors.push('slide'+(i+1));
				}
				i++;
			});

			// setup the wrapper
			wrap.css('height','100%');
			wrap.css('width','100%');
			wrap.css('position','relative');

			// removes the 'bouncing' in various web browsers on scroll (keeps it static)
			$('body,html').css('height','100%')
						  .css('width','100%')
						  .css('overflow','hidden');

		}
		snapSetup();

		// OPTIONAL PAGINATION DOTS
		var setupPagination = function() {
			wrap.append('<div class="snapPagination"><ul></ul></div>');

			var i = 1;
			$('.snap').each(function(){
				$('.snapPagination ul').append('<a href="#'+settings.anchors[i-1]+'"><li class="snapPagination__dot--'+i+'"></li></a>');
				i++;
			});
		}
		if ( settings.navigation ) {
			setupPagination();
		}

		// PAGINATION FUNCTIONALITY

		var paginationCheck = function(active) {
			var num = active.attr('data-snap');
			$('.snapPagination li').removeClass('active');
			$('.snapPagination__dot--'+num).addClass('active');

			if (settings.hideArrows) {
				if(active.hasClass('first')) {
					TweenMax.set('.prevSnap',{autoAlpha:0});
					TweenMax.set('.nextSnap',{autoAlpha:1});
				} else if(active.hasClass('last')) {
					TweenMax.set('.nextSnap',{autoAlpha:0});
					TweenMax.set('.prevSnap',{autoAlpha:1});
				} else {
					TweenMax.set('.prevSnap',{autoAlpha:1});
					TweenMax.set('.nextSnap',{autoAlpha:1});
				}				
			}
		}
		paginationCheck($('.snap:first'));

		// PRIMARY FUNCTIONALITY  (MOUSEWHEEL HANDLER)
		$(window).on('mousewheel', function(event){
			var delta = event.deltaY * event.deltaFactor;
			var current = $('.snap.active');
			var next = current.next();
			var prev = current.prev();

			// ANIMATIONS ON SCROLL DOWN
			if ( delta < -settings.distance && ran == false) {
				ran = true;
				var direction = 'down';

				// check if element is last, if not slide next slide up
				if ( !current.hasClass('last') ) {
					current.removeClass('active');
					next.addClass('active');
					
					if(settings.horizontal) {
						TweenMax.to(next,settings.duration,{x:'0%'});
					} else {
						TweenMax.to(next,settings.duration,{y:'0%'});
					}

					paginationCheck(next);

					// run the onLeave function as you go to the next element
					settings.onLeave(current,direction);

					// runs the afterLoad function AFTER the tween is complete
					setTimeout(function(){
						settings.afterLoad(next,direction);
					},settings.duration*1000);

					// add HREF to URL for pagination purposes
					var slide_num = next.attr('data-snap');
					window.location.hash = settings.anchors[slide_num-1];					
				}

				// after a buffer period allows for scrolling again
				setTimeout(function(){
					ran = false;
				},settings.buffer);
			}

			// ANIMATIONS ON SCROLL UP
			if ( delta > settings.distance && ran == false) {
				ran = true;
				var direction = 'up';
				
				// check if element is first, if not slide current back down
				if ( !current.hasClass('first') ) {
					current.removeClass('active');
					prev.addClass('active');
					
					if(settings.horizontal) {
						TweenMax.to(current,settings.duration,{x:'100%'});	
					} else {
						TweenMax.to(current,settings.duration,{y:'100%'});	
					}

					paginationCheck(prev);

					// run the onLeave function as you scroll to the next element
					settings.onLeave(current,direction);

					// runs the afterLoad function AFTER the tween is complete
					setTimeout(function(){
						settings.afterLoad(next,direction);
					},settings.duration*1000);

					// add HREF to URL for pagination purposes
					var slide_num = prev.attr('data-snap');
					window.location.hash = settings.anchors[slide_num-1];
				}

				// after a buffer period allows for scrolling again
				setTimeout(function(){
					ran = false;
				
					settings.afterLoad(prev,direction);	
				},settings.buffer);	
			}
		});	


		// HREF FUNCTIONALITY
		// Runs once at load and then on hashchange
		var snapHref = function(animate) {

			var hash = window.location.hash.replace('#','');

			for(var i = 0; i < settings.anchors.length; i++) {
				if ( settings.anchors[i] == hash && ran == false) {
					var active = $('[data-snap="'+(i+1)+'"]');
					var prev = active.prevAll('.snap');
					var next = active.nextAll('.snap');
					var current = $('.snap.active');

					// run the onLeave function as you scroll to the next element
					if ( i < current.attr('data-snap')) {
						settings.onLeave(current,'up');
					} else {
						settings.onLeave(current,'down');
					}

					paginationCheck(active);

					$('.snap.active').removeClass('active');
					active.addClass('active');
					
					if(animate) {
						if(settings.horizontal) {
							TweenMax.to(active,settings.duration,{x:'0%'});
							TweenMax.set(prev,{x:'0%'});
							TweenMax.to(next,settings.duration,{x:'100%'});
						} else {
							TweenMax.to(active,settings.duration,{y:'0%'});
							TweenMax.set(prev,{y:'0%'});
							TweenMax.to(next,settings.duration,{y:'100%'});
						}
					} else {
						if(settings.horizontal) {
							TweenMax.set(active,{x:'0%'});
							TweenMax.set(prev,{x:'0%'});
							TweenMax.set(next,{x:'100%'});
						} else {
							TweenMax.set(active,{y:'0%'});
							TweenMax.set(prev,{y:'0%'});
							TweenMax.set(next,{y:'100%'});
						}
					}



					// runs the afterLoad function AFTER the tween is complete
					setTimeout(function(){
						settings.afterLoad(active,'down');
					},settings.duration*1000);

					// add HREF to URL for pagination purposes
					var slide_num = active.attr('data-snap');
					window.location.hash = settings.anchors[slide_num-1];
				}
			}
		}
		snapHref(settings.animateLoad);

		$(window).on('hashchange',function(){
			snapHref(true);
		});

		var nextSnap = function(){
			var current = $('.snap.active');
			var next = current.next();
			var prev = current.prev();

			var direction = 'down';

			// check if element is last, if not slide next slide up
			if ( !current.hasClass('last') ) {
				current.removeClass('active');
				next.addClass('active');
				
				if(settings.horizontal) {
					TweenMax.to(next,settings.duration,{x:'0%'});
				} else {
					TweenMax.to(next,settings.duration,{y:'0%'});
				}

				paginationCheck(next);

				// run the onLeave function as you go to the next element
				settings.onLeave(current,direction);

				// runs the afterLoad function AFTER the tween is complete
				setTimeout(function(){
					settings.afterLoad(next,direction);
				},settings.duration*1000);

				// add HREF to URL for pagination purposes
				var slide_num = next.attr('data-snap');
				window.location.hash = settings.anchors[slide_num-1];					
			}
		}

		var prevSnap = function() {
			var current = $('.snap.active');
			var next = current.next();
			var prev = current.prev();

			var direction = 'up';

			// check if element is first, if not slide current back down
			if ( !current.hasClass('first') ) {
				current.removeClass('active');
				prev.addClass('active');
				
				if(settings.horizontal) {
					TweenMax.to(current,settings.duration,{x:'100%'});	
				} else {
					TweenMax.to(current,settings.duration,{y:'100%'});	
				}

				paginationCheck(prev);

				// run the onLeave function as you scroll to the next element
				settings.onLeave(current,direction);

				// runs the afterLoad function AFTER the tween is complete
				setTimeout(function(){
					settings.afterLoad(next,direction);
				},settings.duration*1000);

				// add HREF to URL for pagination purposes
				var slide_num = prev.attr('data-snap');
				window.location.hash = settings.anchors[slide_num-1];
			}
		}

		// ON CLICK PREV/NEXT
		$('.nextSnap').on('click',function(){
			nextSnap();
		});

		$('.prevSnap').on('click',function(){
			prevSnap();
		});

		// Plugin to Detect Swipe Direction
		// From the comments section at http://blog.blakesimpson.co.uk/read/51-swipe-js-detect-touch-direction-and-distance
		// Creates 'swipeUp', 'swipeDown', 'swipeLeft', and 'swipeRight' jQuery events
		// For mouse dragging functionality, add 'mousedown' to $oElem.on('touchstart' and 'mouseup' to }).on('touchmove

		function initSwipe(iSense,oElem) {

			$.fn.triggerAll = function(events) {
			    if(!events) return this; //don't blow up if .triggerAll() without params
			    var self = this;         //keep a reference
			    $.each(events.split(" "), function(i, e) { self.trigger(e); });
			    return this;
			};

			var xDown = false,
				yDown = false,
				bTouch = 0, 
				fClient = function(e) {
					var oEvt = (bTouch && e.originalEvent && e.originalEvent.touches && e.originalEvent.touches[0])?e.originalEvent.touches[0]:e; 
					return {x:oEvt.clientX,y:oEvt.clientY};
				},
				iSens = (iSense && !isNaN(iSense+1) && iSense > 0)?iSense:4,
				$oElem = $((typeof oElem === 'object')?oElem:document);

			$oElem.on('touchstart', function(e) { 
				if( e && bTouch === 0 ) {
					bTouch = ( e.type === 'touchstart' );
					var oXY = fClient(e);
					xDown = oXY.x; 
					yDown = oXY.y;
				}
				//else { return false; }
			}).on('touchmove', function(e) {
				var bIsMouse = ( e && e.type === 'mouseup' ),
				bIsTouch = !!bTouch; 
				if ( !xDown || !yDown || ( bIsTouch === bIsMouse )) { return; }

				var oXY = fClient(e),
				xDiff = xDown - oXY.x,
				yDiff = yDown - oXY.y,
				bIsX = ( Math.abs( xDiff ) >= iSens ),
				bIsY = ( Math.abs( yDiff ) >= iSens ),
				sEvent = '';

				/* reset values */
				xDown = yDown = false;

				if( bIsX || bIsY ) {
					bTouch = 0; /* reset */
					$(this).triggerAll( 'swipe '+(sEvent = (bIsX?(( xDiff > 0 )?'swipeLeft ':'swipeRight '):'')+(bIsY?(( yDiff > 0 )?'swipeUp':'swipeDown'):'')), 
					[sEvent.replace('swipe','').toLowerCase().split(' '),oXY,e] ); 
				}
			});
		};

		if(settings.touch) {
			initSwipe();

			$(document).on('swipeUp',function(){
				prevSnap();
			});

			$(document).on('swipeDown',function(){
				nextSnap();
			});

			if(settings.horizontal) {
				$(document).on('swipeLeft',function(){
					prevSnap();
				});

				$(document).on('swipeRight',function(){
					nextSnap();
				})
			}
		}
	}
}(jQuery));


// RUNNING THE FUNCTION
// NOT PART OF ACTUAL PLUGIN, WILL TAKE OUT LATER
$('.snapWrap').snappr({
	duration: 1,
	anchors: ['dota','lol'],
	navigation: true,
	horizontal: true,
	afterLoad: function(current,direction) {

	},
	onLeave: function(current,direction) {

	}
});